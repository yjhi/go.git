package jecho

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

type HttpResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type HttpDataResponse struct {
	HttpResponse
	Data interface{} `json:"data"`
}

type HttpListResponse struct {
	HttpResponse
	Data []interface{} `json:"data"`
}

type HttpPageResponse struct {
	PageSize   int           `json:"pageSize"`
	PageNo     int           `json:"pageNo"`
	TotalCount int           `json:"totalCount"`
	TotalPage  int           `json:"totalPage"`
	Data       []interface{} `json:"data"`
}

func ReturnError(c echo.Context, err error) error {

	return c.JSON(http.StatusOK, &HttpResponse{
		Code:    1,
		Message: err.Error(),
	})
}

func ReturnMessage(c echo.Context, code int, msg string) error {
	return c.JSON(http.StatusOK, &HttpResponse{
		Code:    code,
		Message: msg,
	})
}

func ReturnDataMessage(c echo.Context, code int, msg string, data interface{}) error {
	return c.JSON(http.StatusOK, &HttpDataResponse{
		HttpResponse: HttpResponse{
			Code:    code,
			Message: msg,
		},
		Data: data,
	})
}
func ReturnListDataMessage(c echo.Context, code int, msg string, data []interface{}) error {
	return c.JSON(http.StatusOK, &HttpListResponse{
		HttpResponse: HttpResponse{
			Code:    code,
			Message: msg,
		},
		Data: data,
	})
}

type HTTPResponse struct {
	Code    int    `json:"Code"`
	Message string `json:"Message"`
}

type HTTPDataResponse struct {
	HttpResponse
	Data interface{} `json:"Data"`
}

type HTTPListResponse struct {
	HttpResponse
	Data []interface{} `json:"Data"`
}

type HTTPPageResponse struct {
	PageSize   int           `json:"PageSize"`
	PageNo     int           `json:"PageNo"`
	TotalCount int           `json:"TotalCount"`
	TotalPage  int           `json:"TotalPage"`
	Data       []interface{} `json:"Data"`
}

func ReturnErrorU(c echo.Context, err error) error {

	return c.JSON(http.StatusOK, &HTTPResponse{
		Code:    1,
		Message: err.Error(),
	})
}

func ReturnMessageU(c echo.Context, code int, msg string) error {
	return c.JSON(http.StatusOK, &HTTPResponse{
		Code:    code,
		Message: msg,
	})
}

func ReturnDataMessageU(c echo.Context, code int, msg string, data interface{}) error {
	return c.JSON(http.StatusOK, &HTTPDataResponse{
		HttpResponse: HttpResponse{
			Code:    code,
			Message: msg,
		},
		Data: data,
	})
}
func ReturnListDataMessageU(c echo.Context, code int, msg string, data []interface{}) error {
	return c.JSON(http.StatusOK, &HTTPListResponse{
		HttpResponse: HttpResponse{
			Code:    code,
			Message: msg,
		},
		Data: data,
	})
}
