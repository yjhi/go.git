package jecho

import (
	"github.com/labstack/echo/v4"
)

///define http method
type MethodType uint32

const (
	GET MethodType = 1 + iota
	POST
	PUT
	DELETE
)

///define api function
type ControllerMethod struct {
	Method MethodType
	Path   string
	Handle echo.HandlerFunc
}

///defin controller
type Controller struct {
	Context *echo.Echo
	Group   *echo.Group
	_Routes []*ControllerMethod
}

func (c *Controller) RegisterController() {
	if len(c._Routes) > 0 {
		for _, h := range c._Routes {
			switch h.Method {
			case GET:
				c.Group.GET(h.Path, h.Handle)

			case POST:
				c.Group.POST(h.Path, h.Handle)

			case PUT:
				c.Group.PUT(h.Path, h.Handle)

			case DELETE:
				c.Group.DELETE(h.Path, h.Handle)

			}
		}
	}
}

func (c *Controller) AddHandle(m MethodType, p string, h echo.HandlerFunc) {
	f := &ControllerMethod{
		Method: m,
		Path:   p,
		Handle: h,
	}

	c._Routes = append(c._Routes, f)
}

// define interface
type RegisterInterface interface {
	Register(es *EchoServer)
}

// define base controller
type BaseController struct {
	RegisterInterface
	Name string
}

