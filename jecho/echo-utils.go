package jecho

import (
	"github.com/labstack/echo/v4"
)

// 获取请求参数
func TryGetQueryString(c echo.Context, names ...string) string {
	if len(names) == 0 {
		return ""
	}
	for _, name := range names {
		v := c.QueryParam(name)
		if len(v) > 0 {
			return v
		}
	}
	return ""
}
