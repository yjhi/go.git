package jecho

import (
	"fmt"
	"os"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

///define server
type EchoServer struct {
	Server       *echo.Echo
	_Controllers []*Controller
	_IsRegister  bool
}

func (c *EchoServer) RegisterController() {

	if len(c._Controllers) > 0 && !c._IsRegister {
		for _, u := range c._Controllers {
			u.RegisterController()
		}
		c._IsRegister = true
	}
}

func (c *EchoServer) NewController(path string) *Controller {
	cr := &Controller{
		Context: c.Server,
		Group:   c.Server.Group(path),
		_Routes: make([]*ControllerMethod, 0),
	}
	c._Controllers = append(c._Controllers, cr)

	return cr
}
func WriteErrorLog(file, err string) {
	f, e := os.OpenFile(file, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if e != nil {
		fmt.Println(err)
	} else {
		f.WriteString(err)
		f.Close()
	}
}

func (c *EchoServer) Start(host string, port string) {
	c.StartUrl(host + ":" + port)
}

func (c *EchoServer) StartUrl(url string) {
	if !c._IsRegister {
		c.RegisterController()
	}

	c.Server.Logger.Fatal(c.Server.Start(url))
}

///build server
func New() *EchoServer {
	_instance := &EchoServer{
		Server:       echo.New(),
		_Controllers: make([]*Controller, 0),
		_IsRegister:  false,
	}

	_instance.Server.Use(middleware.Recover())

	return _instance
}
