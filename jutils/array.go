/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jutils

//In A not In B
func InAnotB(a []string, b []string) []string {

	key := BuildStringKey(b)
	result := make([]string, 0)

	for _, v := range a {
		if !key.Find(v) {
			result = append(result, v)
		}
	}

	return result
}

// In A and B
func InAInB(a []string, b []string) []string {

	key := BuildStringKey(b)
	result := make([]string, 0)

	for _, v := range a {
		if key.Find(v) {
			result = append(result, v)
		}
	}

	return result
}

// find a in list
func Find(list []string, a string) bool {
	if len(list) == 0 {
		return false
	}

	for _, item := range list {
		if item == a {
			return true
		}
	}
	return false
}
