/*
***************************************************************************
MIT License

# Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
****************************************************************************
*/
package jutils

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
)

/*
*
* add by yjh 211124
* check fiel esists
 */
func FileIsExists(filepath string) bool {
	_, err := os.Lstat(filepath)
	return !os.IsNotExist(err)
}

/*
*
* add by yjh 211124
* read file content
 */
func ReadFileAll(path string) (string, error) {

	if !FileIsExists(path) {
		return "", errors.New("file not found")
	}

	f, err := os.Open(path)
	if err != nil {
		s := fmt.Sprintf("read file err:%s", err.Error())
		return "", errors.New(s)
	}

	defer f.Close()

	fd, err := io.ReadAll(f)
	if err != nil {
		s := fmt.Sprintf("read file content err:%s", err.Error())
		return "", errors.New(s)
	}

	return string(fd), nil
}

// read line
func ReadFileLine(path string, fc func([]byte) bool) error {
	if !FileIsExists(path) {
		return errors.New("file not found")
	}

	f, err := os.Open(path)
	if err != nil {
		s := fmt.Sprintf("read file err:%s", err.Error())
		return errors.New(s)
	}

	defer f.Close()

	rd := bufio.NewReader(f)

	for {
		line, _, e := rd.ReadLine()

		if e == io.EOF {
			break
		}

		if fc(line) {
			break
		}
	}
	return nil
}

// a link writer for writing file
type IFileWriter interface {
	Line(string) IFileWriter
	Lines([]string) IFileWriter
	Close() error
}

type _priFile struct {
	IFileWriter
	_file *os.File
	_err  error
}

func NewFile(file string) IFileWriter {
	f, e := os.OpenFile(file, os.O_WRONLY, 0664)
	return &_priFile{
		_file: f,
		_err:  e,
	}
}
func (p *_priFile) Line(txt string) IFileWriter {
	if p._err == nil {
		_, p._err = p._file.WriteString(txt)
	}
	return p
}

func (p *_priFile) Lines(txt []string) IFileWriter {
	for _, l := range txt {
		p.Line(l)
	}
	return p
}

func (p *_priFile) Close() error {
	return p._err
}

// add by yjh 211124
// make dir
func MkDir(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, 0777)
		os.Chmod(path, 0777)
	}
}

// make dir
func MakeAbsPath(path string, dir string) {

	if len(dir) >= 1 {
		dirs := strings.TrimPrefix(dir, "/")
		dirs = strings.TrimSuffix(dirs, "/")

		if !strings.Contains(dirs, "/") {
			MkDir(path + dir)
			return
		}

		dirList := strings.Split(dirs, "/")
		rootPath := path
		for _, item := range dirList {

			if len(item) > 0 {
				rootPath = rootPath + "/" + item
				MkDir(rootPath)
			}

		}

	}

}

// ParseFileName
func ParseFileName(bfile string) (string, string) {

	fpath := ""
	fname := bfile

	if strings.Contains(bfile, "/") {
		fs := strings.Split(bfile, "/")

		flen := len(fs)
		if flen > 0 {
			fname = fs[flen-1]
			fpath = strings.Join(fs[0:flen-1], "/")
		}
	}
	return fpath, fname
}

// WriteToFile
func WriteToFile(file, err string) {
	f, e := os.OpenFile(file, os.O_CREATE|os.O_WRONLY, 0644)
	if e != nil {
		fmt.Println(err)
	} else {
		f.WriteString(err)
		f.Close()
	}
}

// AppendToFile
func AppendToFile(file, err string) {
	f, e := os.OpenFile(file, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if e != nil {
		fmt.Println(err)
	} else {
		f.WriteString(err)
		f.Close()
	}
}
