package jutils

import (
	"fmt"
	"reflect"
)

// copy filed a-->b
// skip will ignore some filed
func Copy(a interface{}, b interface{}, skip []string) error {
	aType := reflect.TypeOf(a)
	aValue := reflect.ValueOf(a)
	if aType.Kind() == reflect.Ptr {
		aValue = aValue.Elem()
		aType = aType.Elem()
	}

	numA := aValue.NumField()
	if numA == 0 {
		return fmt.Errorf("first param no filed")
	}

	bType := reflect.TypeOf(b)
	bValue := reflect.ValueOf(b)
	if bType.Kind() != reflect.Ptr {
		return fmt.Errorf("second param must be ptr")
	} else {
		bValue = bValue.Elem()
		bType = bType.Elem()
	}

	numB := bValue.NumField()
	if numB == 0 {
		return fmt.Errorf("second param no filed")
	}

	for i := 0; i < numA; i++ {
		n := aType.Field(i)
		v := aValue.Field(i).Interface()

		if n.Type.Kind() == reflect.Pointer {
			continue
		}
		if len(skip) > 0 {
			if Find(skip, n.Name) {
				continue
			}
		}

		for j := 0; j < numB; j++ {
			nb := bType.Field(j)
			if n.Name == nb.Name {
				bValue.Field(j).Set(reflect.ValueOf(v))
				break
			}
		}
	}
	return nil
}

// copy filed a-->b
// filed ignore filed
func CopyFiled(a interface{}, b interface{}, skip []string) error {
	aType := reflect.TypeOf(a)
	aValue := reflect.ValueOf(a)
	if aType.Kind() == reflect.Ptr {
		aValue = aValue.Elem()
		aType = aType.Elem()
	}

	numA := aValue.NumField()
	if numA == 0 {
		return fmt.Errorf("first param no filed")
	}

	bType := reflect.TypeOf(b)
	bValue := reflect.ValueOf(b)
	if bType.Kind() != reflect.Ptr {
		return fmt.Errorf("second param must be ptr")
	} else {
		bValue = bValue.Elem()
		bType = bType.Elem()
	}

	numB := bValue.NumField()
	if numB == 0 {
		return fmt.Errorf("second param no filed")
	}

	for i := 0; i < numA; i++ {
		n := aType.Field(i)
		v := aValue.Field(i).Interface()

		kd := n.Type.Kind()

		if kd == reflect.Pointer ||
			kd == reflect.Array ||
			kd == reflect.Map ||
			kd == reflect.Slice ||
			kd == reflect.Struct ||
			kd == reflect.Uintptr ||
			kd == reflect.Chan ||
			kd == reflect.Func ||
			kd == reflect.UnsafePointer ||
			kd == reflect.Interface {
			continue
		}
		if len(skip) > 0 {
			if Find(skip, n.Name) {
				continue
			}
		}

		for j := 0; j < numB; j++ {
			nb := bType.Field(j)
			if n.Name == nb.Name {
				bValue.Field(j).Set(reflect.ValueOf(v))
				break
			}
		}
	}
	return nil
}
