package jservice

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"gitee.com/yjhi/go/jutils"
)

type LinuxService struct {
	BaseService
}

const (
	linuxInstall = "/lib/systemd/system"
)

const (
	Linux_Server_Net  = "network.target"
	Linux_Server_Sshd = "sshd.service"
)

func (s LinuxService) _ServiceFile() string {
	file := linuxInstall + "/" + s.ExeName + ".service"
	return file
}
func (s LinuxService) MakeService() {
	file := s._ServiceFile()
	f, _ := os.OpenFile(file, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0644)

	sv := ""
	if len(s.After) > 0 {
		sv = strings.Join(s.After, " ")
	}
	strFmt := `
[Unit]
Description=%s service
After=network.target %s
[Service]
User=root
Group=root
Restart=on-failure
ExecStart=%s%s
WorkingDirectory=%s

[Install]
WantedBy=multi-user.target
`
	if len(s.Params) > 0 {
		f.WriteString(fmt.Sprintf(strFmt, s.ExeName, sv, s.ExeFile, " "+s.Params, s.ExePath))
	} else {
		f.WriteString(fmt.Sprintf(strFmt, s.ExeName, sv, s.ExeFile, "", s.ExePath))
	}

	f.Close()
}

func (s LinuxService) Install() (string, error) {
	file := s._ServiceFile()
	if jutils.FileIsExists(file) {
		return "", errors.New("文件" + file + "已存在")
	}

	s.MakeService()

	ss, se := s.RunCmd("systemctl", "enable", s.ExeName)

	if se != nil {
		fmt.Println("创建服务" + s.ExeName + "失败")
	} else {
		fmt.Println("创建服务" + s.ExeName + "成功")
	}

	fmt.Println(ss)

	return ss, se
}

func (s LinuxService) UnInstall() (string, error) {
	ss, se := s.RunCmd("systemctl", "disable", s.ExeName)

	if se != nil {
		fmt.Println("删除服务" + s.ExeName + "失败")
	} else {
		fmt.Println("除服务" + s.ExeName + "成功")
	}
	fmt.Println(ss)
	return ss, se
}
func (s LinuxService) Start() (string, error) {
	ss, se := s.RunCmd("systemctl", "start", s.ExeName)
	fmt.Println(ss)
	return ss, se
}
func (s LinuxService) Stop() (string, error) {
	ss, se := s.RunCmd("systemctl", "stop", s.ExeName)
	fmt.Println(ss)
	return ss, se
}

func (s LinuxService) AddAfter(n string) {
	if s.After == nil {
		s.After = make([]string, 0)
	}

	s.After = append(s.After, n)
}

func (s LinuxService) Reload() (string, error) {
	ss, se := s.RunCmd("systemctl", "daemon-reload")
	fmt.Println(ss)
	return ss, se
}
