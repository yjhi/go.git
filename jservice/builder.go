package jservice

import (
	"fmt"
	"path"
	"runtime"
	"strings"
)

func New(file string, params string) (IService, error) {
	systemType := runtime.GOOS

	rawExeFile := file
	exeFile := strings.Replace(rawExeFile, "\\", "/", -1)

	exeDir := path.Dir(exeFile)
	exeName := path.Base(exeFile)
	exeName = strings.Replace(exeName, ".exe", "", 1)

	if systemType == "windows" {
		return &WinService{
			BaseService{
				ExeFile: exeFile,
				ExeName: exeName,
				ExePath: exeDir,
				Params:  params,
			},
		}, nil
	} else if systemType == "linux" {
		return &LinuxService{
			BaseService{
				ExeFile: exeFile,
				ExeName: exeName,
				ExePath: exeDir,
				Params:  params,
			},
		}, nil
	} else {
		return nil, fmt.Errorf("System Not Support")
	}
}
