package jservice

import (
	"fmt"
)

type WinService struct {
	BaseService
}

func (s WinService) Install() (string, error) {
	p := ""
	if len(s.Params) > 0 {
		p = " " + s.Params
	}

	ss, se := s.RunCmdGbk("sc", "create", s.ExeName, "binPath=", s.ExeFile+p, "start=", "auto", "DisplayName=", s.ExeName)

	if se != nil {
		fmt.Println("创建服务" + s.ExeName + "失败")
	} else {
		fmt.Println("创建服务" + s.ExeName + "成功")
		s.RunCmdGbk("sc", "description", s.ExeName, s.ExeName+"服务")
	}

	fmt.Println(ss)

	return ss, se
}

func (s WinService) UnInstall() (string, error) {
	ss, se := s.RunCmdGbk("sc", "delete", s.ExeName)

	if se != nil {
		fmt.Println("删除服务" + s.ExeName + "失败")
	} else {
		fmt.Println("除服务" + s.ExeName + "成功")
	}
	fmt.Println(ss)
	return ss, se
}
func (s WinService) Start() (string, error) {
	ss, se := s.RunCmdGbk("net", "start", s.ExeName)
	fmt.Println(ss)
	return ss, se
}
func (s WinService) Stop() (string, error) {
	ss, se := s.RunCmdGbk("net", "stop", s.ExeName)
	fmt.Println(ss)
	return ss, se
}

func (s WinService) AddAfter(n string) {

}
func (s WinService) Reload() (string, error) {
	return "", nil
}
