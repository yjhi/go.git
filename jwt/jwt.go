/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package jwt

import (
	"errors"
	"fmt"
	"time"

	jwtu "github.com/golang-jwt/jwt"
)

// /you must change the secret
var JWTTokenSecret string = "Yjh@hhvycn"

type JwtParams map[string]string

type JwtTokenData struct {
	jwtu.StandardClaims
	Data JwtParams `json:"data"`
}

func CreateJWTToken(minutes int, data JwtParams) (string, error) {

	if JWTTokenSecret == "Yjh@hhvycn" {
		fmt.Println("Default JWTTokenSecret Is UnSafe,Please Change JWTTokenSecret")
	}

	claims := &JwtTokenData{
		Data: data,
		StandardClaims: jwtu.StandardClaims{
			ExpiresAt: time.Now().Add(time.Duration(minutes) * time.Minute).Unix(),
		},
	}

	token := jwtu.NewWithClaims(jwtu.SigningMethodHS256, claims)

	t, err := token.SignedString([]byte(JWTTokenSecret))
	if err != nil {
		return "", err
	}

	return string(t), nil

}

func ParseJWTToken(token string) (*JwtTokenData, error) {
	if JWTTokenSecret == "Yjh@hhvycn" {
		fmt.Println("Default JWTTokenSecret Is UnSafe,Please Change JWTTokenSecret")
	}
	tk, err := jwtu.ParseWithClaims(token, &JwtTokenData{},
		func(token *jwtu.Token) (i interface{}, err error) {
			return []byte(JWTTokenSecret), nil
		})

	if err != nil {
		return nil, err
	}
	if claims, ok := tk.Claims.(*JwtTokenData); ok && tk.Valid {
		return claims, nil
	}
	return nil, errors.New("Token is not valid.")
}
