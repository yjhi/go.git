package jtask

import "gitee.com/yjhi/go/jutils"

///define data job
type CronDataTaskJob struct {
	Time    string
	TaskId  string
	WorkFun func(s string, data interface{})
	RunId   int
	Data    interface{}
}

///define the task
func (ct *CronDataTaskJob) Run() {
	go ct.WorkFun(ct.TaskId, ct.Data)
}

func NewDataJob(time string, workFun func(s string, data interface{}), data interface{}) *CronDataTaskJob {

	return &CronDataTaskJob{
		Time:    time,
		TaskId:  jutils.NewGuid(),
		WorkFun: workFun,
		Data:    data,
	}
}
