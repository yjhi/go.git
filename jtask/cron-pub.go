package jtask

import "github.com/robfig/cron/v3"

///cron manager builder
func New(seconds bool) *CronTask {

	if seconds {
		return &CronTask{
			CronInstance: cron.New(cron.WithSeconds()),
			WithSeconds:  seconds,
			_Started:     false,
			_Stoped:      false,
		}
	}
	return &CronTask{
		CronInstance: cron.New(),
		WithSeconds:  seconds,
		_Started:     false,
		_Stoped:      false,
	}
}
