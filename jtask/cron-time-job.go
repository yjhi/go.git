package jtask

import (
	"fmt"
	"time"

	"gitee.com/yjhi/go/jutils"
)

func Now() time.Time {
	return time.Now()
}

///define data job
type CronTimeTaskJob struct {
	CronDataTaskJob
	runTime time.Time
}

///define the task
func (ct *CronTimeTaskJob) Run() {
	n := Now()

	if ct.runTime.Year() == n.Year() {
		go ct.WorkFun(ct.TaskId, ct.Data)
	}
}

func NewTimeJob(time time.Time, workFun func(s string, data interface{}), data interface{}) *CronTimeTaskJob {

	return &CronTimeTaskJob{
		CronDataTaskJob: CronDataTaskJob{
			Time:    fmt.Sprintf("%d %d %d %d %d *", time.Second(), time.Minute(), time.Hour(), time.Day(), time.Month()),
			TaskId:  jutils.NewGuid(),
			WorkFun: workFun,
			Data:    data,
		},
		runTime: time,
	}
}
