module gitee.com/yjhi/go/jtask

go 1.19

require (
	gitee.com/yjhi/go/jutils v0.0.0-20240808094953-21d66f2d0abd
	github.com/robfig/cron/v3 v3.0.1
)

require (
	github.com/google/uuid v1.6.0 // indirect
	golang.org/x/text v0.16.0 // indirect
)
