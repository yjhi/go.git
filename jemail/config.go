package jemail

func buidEmail(t int8) *EmailServer {
	switch t {
	case Email_Outlook:
		return &EmailServer{
			Server:    "smtp-mail.outlook.com",
			Port:      "587",
			PopServer: "outlook.office365.com",
			PopPort:   "993",
		}
	case Email_QQ:
		return &EmailServer{
			Server: "smtp.qq.com",
			Port:   "25",
		}
	case Email_Qywx:
		return &EmailServer{
			Server:    "smtp.exmail.qq.com",
			Port:      "465",
			PopServer: "imap.exmail.qq.com",
			PopPort:   "993",
		}
	case Email_126:
		return &EmailServer{
			Server: "smtp.126.com",
			Port:   "25",
		}
	case Email_163:
		return &EmailServer{
			Server: "smtp.163.com",
			Port:   "25",
		}
	default:
		return &EmailServer{
			Server: "smtp.163.com",
			Port:   "25",
		}
	}
}
