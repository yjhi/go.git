package jemail

import (
	"crypto/tls"
	"net/smtp"

	"github.com/jordan-wright/email"
)

type EmailUtils struct {
	_email    *email.Email
	_account  string
	_password string
	_server   *EmailServer
	_receiver []string
}
type EmailServer struct {
	Server string
	Port   string

	PopServer string
	PopPort   string
}

func (e *EmailServer) Host() string {
	return e.Server + ":" + e.Port
}

const (
	Email_QQ int8 = 1 + iota
	Email_163
	Email_126
	Email_Outlook
	Email_Qywx
)

func New() *EmailUtils {
	return &EmailUtils{
		_email:    email.NewEmail(),
		_receiver: make([]string, 0),
	}
}

func (s *EmailUtils) WithSender(from string, account string, password string) *EmailUtils {
	s._email.From = from
	s._account = account
	s._password = password
	return s
}
func (s *EmailUtils) WithServer(t int8) *EmailUtils {
	s._server = buidEmail(t)
	return s
}

func (s *EmailUtils) WithServerConfig(server string, port string, popServer string, popPort string) *EmailUtils {
	s._server = &EmailServer{
		Server:    server,
		Port:      port,
		PopServer: popServer,
		PopPort:   popPort,
	}
	return s
}

func (s *EmailUtils) WithReceiver(receiver string) *EmailUtils {
	s._receiver = append(s._receiver, receiver)
	return s
}

func (e *EmailUtils) Send(sub string, txt []byte) error {
	e._email.Subject = sub
	e._email.Text = txt
	e._email.To = e._receiver

	return e._email.Send(e._server.Host(), smtp.PlainAuth("", e._account, e._password, e._server.Server))
}

func (e *EmailUtils) SendWithSTL(sub string, txt []byte) error {
	e._email.Subject = sub
	e._email.Text = txt
	e._email.To = e._receiver

	return e._email.SendWithTLS(e._server.Host(),
		smtp.PlainAuth("", e._account, e._password, e._server.Server),
		&tls.Config{ServerName: e._server.Server})
}

func (e *EmailUtils) BatchSend() error {
	return nil
}
