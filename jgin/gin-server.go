package jgin

import (
	"fmt"
	"net/http"

	"gitee.com/yjhi/go/jtime"
	"gitee.com/yjhi/go/jutils"
	"github.com/gin-gonic/gin"
)

///define server
type GinServer struct {
	Server       *gin.Engine
	_Controllers []*Controller
	_IsRegister  bool
}

func Recover(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			errStr := fmt.Sprintf("\n------->\n%s\n程序出现错误:%v\n------->\n", jtime.NowDateTime(), err)
			jutils.AppendToFile("error.log", errStr)
			c.JSON(http.StatusOK, gin.H{
				"code":    1,
				"message": fmt.Sprintf("%v", err),
			})
		}
	}()
	c.Next()
}

func (c *GinServer) RegisterController() {

	if len(c._Controllers) > 0 && !c._IsRegister {
		for _, u := range c._Controllers {
			u.RegisterController()
		}
		c._IsRegister = true
	}
}

// release mode
func (c *GinServer) WithRelease() *GinServer {
	gin.SetMode(gin.ReleaseMode)
	return c
}

// new controller
func (c *GinServer) NewController(path string) *Controller {
	cr := &Controller{
		Server:  c.Server,
		Group:   c.Server.Group(path),
		_Routes: make([]*ControllerMethod, 0),
	}
	c._Controllers = append(c._Controllers, cr)

	return cr
}

// start server with ip and port
func (c *GinServer) Start(host string, port string) {
	c.StartUrl(host + ":" + port)
}

func (c *GinServer) StartUrl(url string) {
	if !c._IsRegister {
		c.RegisterController()
	}
	c.Server.Run(url)
}

///build server
func New() *GinServer {
	_instance := &GinServer{
		Server:       gin.New(),
		_Controllers: make([]*Controller, 0),
		_IsRegister:  false,
	}
	_instance.Server.Use(Recover)
	return _instance
}
