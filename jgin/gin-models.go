package jgin

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type HttpResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type HttpDataResponse struct {
	HttpResponse
	Data interface{} `json:"data"`
}

type HttpListResponse struct {
	HttpResponse
	Data interface{} `json:"data"`
}

type HttpPageResponse struct {
	HttpResponse
	PageSize   int         `json:"pageSize"`
	PageNo     int         `json:"pageNo"`
	TotalCount int         `json:"totalCount"`
	TotalPage  int         `json:"totalPage"`
	Data       interface{} `json:"data"`
}

func ReturnError(c *gin.Context, err error) {
	c.JSON(http.StatusOK, &HttpResponse{
		Code:    1,
		Message: err.Error(),
	})
}

func ReturnMessage(c *gin.Context, code int, msg string) {
	c.JSON(http.StatusOK, &HttpResponse{
		Code:    code,
		Message: msg,
	})
}

func ReturnDataMessage(c *gin.Context, code int, msg string, data interface{}) {
	c.JSON(http.StatusOK, &HttpDataResponse{
		HttpResponse: HttpResponse{
			Code:    code,
			Message: msg,
		},
		Data: data,
	})
}
func ReturnListDataMessage(c *gin.Context, code int, msg string, data interface{}) {
	c.JSON(http.StatusOK, &HttpListResponse{
		HttpResponse: HttpResponse{
			Code:    code,
			Message: msg,
		},
		Data: data,
	})
}

type HTTPResponse struct {
	Code    int    `json:"Code"`
	Message string `json:"Message"`
}

type HTTPDataResponse struct {
	HTTPResponse
	Data interface{} `json:"Data"`
}

type HTTPListResponse struct {
	HTTPResponse
	Count int         `json:"Count"`
	Data  interface{} `json:"Data"`
}

type HTTPPageResponse struct {
	HTTPResponse
	PageSize   int         `json:"PageSize"`
	PageNo     int         `json:"PageNo"`
	TotalCount int         `json:"TotalCount"`
	TotalPage  int         `json:"TotalPage"`
	Data       interface{} `json:"Data"`
}

func ReturnErrorU(c *gin.Context, err error) {

	c.JSON(http.StatusOK, &HTTPResponse{
		Code:    1,
		Message: err.Error(),
	})
}

func ReturnMessageU(c *gin.Context, code int, msg string) {
	c.JSON(http.StatusOK, &HTTPResponse{
		Code:    code,
		Message: msg,
	})
}

func ReturnDataMessageU(c *gin.Context, code int, msg string, data interface{}) {
	c.JSON(http.StatusOK, &HTTPDataResponse{
		HTTPResponse: HTTPResponse{
			Code:    code,
			Message: msg,
		},
		Data: data,
	})
}
func ReturnListDataMessageU(c *gin.Context, code int, msg string, data interface{}) {
	c.JSON(http.StatusOK, &HTTPListResponse{
		HTTPResponse: HTTPResponse{
			Code:    code,
			Message: msg,
		},
		Data: data,
	})
}
