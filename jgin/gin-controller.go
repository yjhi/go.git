package jgin

import (
	"github.com/gin-gonic/gin"
)

///define http method
type MethodType uint32

const (
	GET MethodType = 1 + iota
	POST
	PUT
	DELETE
)

///define api function
type ControllerMethod struct {
	Method MethodType
	Path   string
	Handle gin.HandlerFunc
}

///defin controller
type Controller struct {
	Server  *gin.Engine
	Group   *gin.RouterGroup
	_Routes []*ControllerMethod
}

func (c *Controller) RegisterController() {
	if len(c._Routes) > 0 {
		for _, h := range c._Routes {
			switch h.Method {
			case GET:
				c.Group.GET(h.Path, h.Handle)

			case POST:
				c.Group.POST(h.Path, h.Handle)

			case PUT:
				c.Group.PUT(h.Path, h.Handle)

			case DELETE:
				c.Group.DELETE(h.Path, h.Handle)

			}
		}
	}
}

func (c *Controller) AddHandle(m MethodType, p string, h gin.HandlerFunc) {
	f := &ControllerMethod{
		Method: m,
		Path:   p,
		Handle: h,
	}

	c._Routes = append(c._Routes, f)
}
