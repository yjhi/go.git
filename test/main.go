/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/

package main

import (
	"fmt"

	jkv "gitee.com/yjhi/go/jkv"
	"gitee.com/yjhi/go/jlog"
	"gitee.com/yjhi/go/jutils"
)

type TestA struct {
	A string
	B string
	C int
	D int
	F float32
}

func main() {

	log := jlog.BuildMultLog(".", "test", "txt", jlog.DEBUG)

	// cc := jtime.BuildTimeCount()
	// for i := 0; i < 500000; i++ {
	// 	log.Debug("11111111111111111111" + fmt.Sprintf("%d", i))
	// }
	// fmt.Printf("TT1=%f\n", cc.Seconds())

	log.Error("11111111111111111111")
	log.LogF(jlog.ERROR, "{0}={1}+{2},\t{3},{0}", "yyy", "234", "23.23", "AAA")

	defer log.Close()

	ss := jlog.FormatLogs("{0}={1}+{2},\t{3},{0}", "yyy", "234", "23.23", "AAA")
	fmt.Println(ss)

	//jkv
	kv := jkv.New("data/cache")

	kv.Open()

	fmt.Println(kv.SetJson("b", TestA{
		A: "AAAA",
		B: "BBBB",
		C: 34,
		D: 23,
		F: 23.56,
	}))

	var d TestA

	fmt.Println(kv.GetJson("b", &d))

	fmt.Println(d)
	//

	//jfile
	f := jutils.NewFile("test.log")

	f.Line("21111111111111")

	f.Lines([]string{"11111111111", "2222222222222"})

	f.Close()

}
