module test

go 1.19

require (
	gitee.com/yjhi/go/jkv v0.0.0-20240808093403-99eb5579b08b
	gitee.com/yjhi/go/jlog v0.0.0-20240808012704-30ca75c7073d
	gitee.com/yjhi/jutils v1.0.0
)

require (
	gitee.com/yjhi/go/jtime v0.0.0-20240606155114-5b5fd8c14f1e // indirect
	gitee.com/yjhi/go/jutils v0.0.0-20240808095829-06146d5a97ff // indirect
	github.com/golang/snappy v0.0.0-20180518054509-2e65f85255db // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	golang.org/x/text v0.17.0 // indirect
)
