/*
***************************************************************************
MIT License

# Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
****************************************************************************
*/
package jlog

import (
	"encoding/json"
)

type LogConfig struct {
	LogPath string
	LogName string
	LogTail string

	Level      int8
	Buffer     bool
	FlushCount int

	LogType int8

	Handler LogHandler
}

// LogUtils
// add by yjh 211123
type LogUtils struct {
	LogConfig
	LastError error
	logPri
}

func (l *LogUtils) WithLevel(level int8) *LogUtils {
	l.Level = level
	return l
}

// hook
func (l *LogUtils) Use(fun LogHandler) {
	l.Handler = fun
}

func (l *LogUtils) Flush() {
	if l != nil {
		if l._writer != nil {
			l._writer.Flush()
		}
		if l._logFile != nil {
			l._logFile.Sync()
		}
	}

}

// close file
func (l *LogUtils) Close() {
	if l != nil {

		l.Flush()

		if l._logFile != nil {
			l._logFile.Close()
		}
	}
}

func (l *LogUtils) LogFileName() string {
	return l._buildLogFullPath()
}

// debug
func (l *LogUtils) Debug(content string) {
	l._writeLevelLog(DEBUG, content)

}

// info
func (l *LogUtils) Info(content string) {
	l._writeLevelLog(INFO, content)

}

// warn
func (l *LogUtils) Warn(content string) {
	l._writeLevelLog(WARN, content)

}

// error
func (l *LogUtils) Error(content string) {
	l._writeLevelLog(ERROR, content)
}

// critical
func (l *LogUtils) Critical(content string) {
	l._writeLevelLog(CRITICAL, content)
}

// log
func (l *LogUtils) Log(level int8, content string) {
	l._writeLevelLog(level, content)
}

// logs
func (l *LogUtils) Logs(level int8, content ...string) {
	if level <= l.Level {
		l._writeLog(GetLevelName(level), LogFormat(content...))
	}
}

func (l *LogUtils) LogJson(level int8, content interface{}) {
	if level <= l.Level {
		retStr, retErr := json.Marshal(content)
		if retErr != nil {
			l._writeLog(GetLevelName(level), retErr.Error())

		} else {
			l._writeLog(GetLevelName(level), string(retStr))
		}
	}
}

func (l *LogUtils) LogV(level int8, content ...interface{}) {
	if level <= l.Level {
		l._writeLog(GetLevelName(level), LogFormatV(content...))
	}
}

func (l *LogUtils) WriteLevel(level int8) *LogUtils {
	if level <= l.Level {
		l._writeRawLog(GetLevelName(level), "")
	}
	return l
}

func (l *LogUtils) WriteContent(content string) *LogUtils {
	l._writeRawLog("", content)
	return l
}

func (l *LogUtils) Write(p []byte) (int, error) {
	l._checkAndOpenFile()
	n, er := l._writer.Write(p)

	return n, er
}

// /format {0} {1} {2}
func (l *LogUtils) LogF(level int8, format string, content ...string) *LogUtils {
	if level <= l.Level {
		l._writeLog(GetLevelName(level), FormatLogs(format, content...))
	}
	return l
}
