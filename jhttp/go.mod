module gitee.com/yjhi/go/jhttp

go 1.19

require gitee.com/yjhi/go/jutils v0.0.0-20240808094953-21d66f2d0abd

require (
	github.com/google/uuid v1.6.0 // indirect
	golang.org/x/text v0.16.0 // indirect
)
