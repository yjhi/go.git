package jhttp

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"strings"
	"time"

	"bufio"

	"gitee.com/yjhi/go/jutils"
)

// 保存文件
func SaveFile(file *multipart.FileHeader, path string, name string) (string, error) {
	rd, rdEr := file.Open()
	if rdEr != nil {
		return "", rdEr
	}
	defer rd.Close()

	imgName := file.Filename
	if len(name) > 0 {
		imgName = name
	}

	wt, err := os.Create(path + imgName)
	if err != nil {
		return "", err
	}
	defer wt.Close()

	if _, err = io.Copy(wt, rd); err != nil {

		return "", err
	}

	return imgName, nil
}

// download file
func DownFile(url string, fname string, fpath string) error {

	down := CreateGET(url, 0)

	resp, err := down.SendRequestWithResp()

	if err != nil {
		return err
	}

	res := resp.Response

	if res.StatusCode != 200 {
		return errors.New(fmt.Sprintf("err status:%d", res.StatusCode))
	}

	defer res.Body.Close()

	path := fpath + "/" + fname
	reader := bufio.NewReaderSize(res.Body, 64*1024)

	file, createErr := os.Create(path)
	if createErr != nil {
		return createErr
	}

	writer := bufio.NewWriter(file)
	_, copyErr := io.Copy(writer, reader)
	if copyErr != nil {
		file.Close()
		return copyErr
	}

	file.Close()

	return nil
}

// upload file to server,name is files
func UploadFile(url string, timeout int32, filePath string) (string, error) {
	return UploadFiles(url, timeout, []string{filePath}, "files")
}

// upload file to server
func UploadFileWithName(url string, timeout int32, filePath string, name string) (string, error) {
	return UploadFiles(url, timeout, []string{filePath}, name)
}

// upload files to url
func UploadFiles(url string, timeout int32, files []string, name string) (string, error) {
	var c *http.Client = nil

	if timeout > 0 {
		c = &http.Client{
			Timeout: time.Second * time.Duration(timeout),
		}
	} else {
		c = &http.Client{
			Timeout: 0,
		}
	}

	boundary := "---" + jutils.NewGuid()

	var picData strings.Builder

	var errStr strings.Builder

	for _, filePath := range files {
		var data []byte
		if _, err := os.Lstat(filePath); err == nil {
			file, _ := os.Open(filePath)
			defer file.Close()

			data, _ = io.ReadAll(file)

		} else {
			errStr.WriteString("file " + filePath + " not exist\n")
			continue
		}

		_, fname := jutils.ParseFileName(filePath)

		picData.WriteString("--" + boundary + "\n")
		picData.WriteString("Content-Disposition: form-data; name=\"" + name + "\"; filename=\"" + fname + "\"\n")
		picData.WriteString("Content-Type: application/octet-stream\n\n")
		picData.Write(data)
	}
	picData.WriteString("\n--" + boundary + "--")

	if errStr.Len() > 0 {
		return "", errors.New(errStr.String())
	}

	req, err := http.NewRequest("POST", url, strings.NewReader(picData.String()))

	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", "multipart/form-data; boundary="+boundary)

	if rep, err1 := c.Do(req); err1 == nil {
		content, _ := ioutil.ReadAll(rep.Body)
		rep.Body.Close()
		return string(content), nil
	} else {
		return "", err1
	}
}
