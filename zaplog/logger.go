package zaplog

import (
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

func (z *ZapLogger) Logger() *zap.Logger {

	if z.config == nil {
		z.config = DefaultConfig()
	}

	if z.logger == nil {

		var encoder zapcore.Encoder
		if z.config.Json { //如果是json格式
			encoder = zapcore.NewJSONEncoder(z.EncoderConfig)
		} else {
			encoder = zapcore.NewConsoleEncoder(z.EncoderConfig)
		}

		core := zapcore.NewCore(
			encoder,                         //编码设置
			zapcore.AddSync(z.RotateLogger), //输出到文件
			z.config.Level,                  //日志等级
		)

		z.logger = zap.New(core)
		if z.config.WithCaller {
			z.logger = z.logger.WithOptions(zap.AddCaller())
		}

		if z.config.WithStack {
			z.logger = z.logger.WithOptions(zap.AddStacktrace(z.config.StackLevel))
		}

	}
	return z.logger
}

// Close 关闭日志文件
func (z *ZapLogger) Close() {
	if z != nil {
		z.RotateLogger.Close()
		z.logger.Sync()
	}
}

// Log 输出日志
func (z *ZapLogger) Info(msgs ...string) {
	if z != nil && z.logger != nil {
		z.logger.Info(LogFormat(msgs...))
	}
}

// Log 输出日志
func (z *ZapLogger) Debug(msgs ...string) {
	if z != nil && z.logger != nil {
		z.logger.Debug(LogFormat(msgs...))
	}
}

// Log 输出日志
func (z *ZapLogger) Error(msgs ...string) {
	if z != nil && z.logger != nil {
		z.logger.Error(LogFormat(msgs...))
	}
}

// Log 输出日志
func (z *ZapLogger) Warn(msgs ...string) {
	if z != nil && z.logger != nil {
		z.logger.Warn(LogFormat(msgs...))
	}
}

func (z *ZapLogger) LogErr(name string, msg error) {
	if z != nil && z.logger != nil {
		z.logger.Error(name + ":" + msg.Error())
	}
}

func (z *ZapLogger) LogV(name string, v interface{}) {
	if z != nil && z.logger != nil {
		z.logger.Error(name + ":" + fmt.Sprintf("%+v", v))
	}
}

func NewZapLogger(conf *ZapLoggerConfig) *ZapLogger {

	izap := &ZapLogger{
		RotateLogger: &lumberjack.Logger{
			Filename:   conf.Filename,
			MaxSize:    conf.MaxSize, // megabytes
			MaxBackups: conf.MaxBackups,
			MaxAge:     conf.MaxAge,   //days
			Compress:   conf.Compress, // disabled by default
		},
		EncoderConfig: zap.NewProductionEncoderConfig(),
		config:        conf,
	}

	izap.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder // 设置时间格式
	izap.EncoderConfig.MessageKey = "content"
	izap.EncoderConfig.TimeKey = "time"

	return izap
}

func DefaultConfig() *ZapLoggerConfig {
	return &ZapLoggerConfig{
		Filename:   "./app.log",
		MaxSize:    20,
		MaxBackups: 0,
		MaxAge:     15,
		Compress:   true,
		Json:       false,
		WithCaller: true,
		WithStack:  true,
		Level:      zapcore.InfoLevel,
		StackLevel: zapcore.WarnLevel,
	}
}
