package zaplog

import (
	"fmt"
	"strings"
)

func LogFormatVWithCut(cut string, content ...interface{}) string {
	var b strings.Builder

	for _, v := range content {
		b.WriteString(fmt.Sprintf("%+v%s", v, cut))
	}
	return b.String()
}

func LogFormatV(content ...interface{}) string {
	return LogFormatVWithCut("\t", content...)
}

func LogFormatWithCut(cut string, content ...string) string {
	var b strings.Builder
	for _, v := range content {
		b.WriteString(v + cut)
	}
	return b.String()
}

func LogFormat(content ...string) string {
	return LogFormatWithCut("\t", content...)
}

// format log
// format {0} {1} {2}
func FormatLogs(format string, content ...string) string {
	ret := format
	for inx, s := range content {
		ret = strings.Replace(ret, fmt.Sprintf("{%d}", inx), s, -1)
	}
	return ret
}
