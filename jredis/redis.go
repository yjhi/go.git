/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/
package jredis

import (
	"strconv"

	"github.com/gomodule/redigo/redis"
)

type RedisConfig struct {
	Host     string
	Port     int
	Password string
	Index    int
}

type RedisPoolConfig struct {
	RedisConfig
	MaxIdle     int
	MaxActive   int
	IdleTimeout int
}

/// define redis wrap
type RedisClient struct {
	RedisConn redis.Conn
	RedisConfig
}

/// connect redis
func (r *RedisClient) Connect() error {
	var er error

	r.RedisConn, er = _CreateRedisConnect(&r.RedisConfig)

	if er != nil {
		return er
	}

	return nil
}

func (r *RedisClient) WithHost(host string) *RedisClient {
	r.Host = host
	return r
}
func (r *RedisClient) WithPort(port int) *RedisClient {
	r.Port = port
	return r
}

func (r *RedisClient) WithPortStr(port string) *RedisClient {

	p, e := strconv.Atoi(port)

	if e != nil {
		p = 6379
	}
	r.Port = p
	return r
}

func (r *RedisClient) WithPassword(pass string) *RedisClient {
	r.Password = pass
	return r
}

func (r *RedisClient) WithDb(index int) *RedisClient {
	r.Index = index
	return r
}

//new redis instance
func New() *RedisClient {
	return &RedisClient{
		RedisConfig: RedisConfig{
			Host:     "127.0.0.1",
			Port:     6379,
			Password: "",
			Index:    0,
		},
	}
}

func NewWithConfig(cfg RedisConfig) *RedisClient {
	return &RedisClient{
		RedisConfig: RedisConfig{
			Host:     cfg.Host,
			Port:     cfg.Port,
			Password: cfg.Password,
			Index:    cfg.Index,
		},
	}
}
