/****************************************************************************
MIT License

Copyright (c) 2022 yjhi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*****************************************************************************/
package jredis

import (
	"fmt"

	"github.com/gomodule/redigo/redis"
)

func _CreateRedisConnect(r *RedisConfig) (redis.Conn, error) {
	setDb := r.Index > 0
	setPass := len(r.Password) > 0

	if !setDb && !setPass {
		return redis.Dial("tcp", r.Host+":"+fmt.Sprintf("%d", r.Port))
	} else if setDb && setPass {
		return redis.Dial("tcp", r.Host+":"+fmt.Sprintf("%d", r.Port), redis.DialDatabase(r.Index), redis.DialPassword(r.Password))
	} else if setDb {
		return redis.Dial("tcp", r.Host+":"+fmt.Sprintf("%d", r.Port), redis.DialDatabase(r.Index))
	} else {
		return redis.Dial("tcp", r.Host+":"+fmt.Sprintf("%d", r.Port), redis.DialPassword(r.Password))
	}
}
