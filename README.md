# golib

this is a go lib.

### jtime

```go
//example1
timeUtil:=BuildTimeUtils()
```


### jhttp

```go
//example1
client := jhttp.CreateGet(url, timeout)
if client == nil {
	fmt.Println("ERR:Client NIL")
	return
}
client.AetHeader("User-Agent", strNowUA).AddCookie("name", value, "/", domain)
body, err := client.SendRequest()
if err != nil {
	return ""
}

......

//example2
client := jhttp.CreateGet("https://v.sogou.com/?forceredirect=2&ie=utf8", timeout)

if client == nil {
	fmt.Println("ERR:Client null")
	return
}

client.AetHeader("User-Agent", strNowUA).AddCookie("name", value, "/", domain)
resp, err := client.SendRequestWithResp()

if err != nil {
	fmt.Println("ERR:" + err.Error())
	return
}

for _, cookie := range resp.Response.Cookies() {
    ......
}

```

### jbase62

```go

en:=NewEncoding()
s:=en.ToBase62(79876545352)
i:=en.FromBase62("uyiouj8")


//example
	t := jtime.New()

	en := NewEncoding()

	ok := 0
	err := 0

	fmt.Println(t.GetDateTime())

	for i := 10000000; i > 0; i-- {

		s := en.ToBase62(int64(i))

		i1 := en.FromBase62(s)

		if i1 == int64(i) {
			ok++
		} else {
			err++
		}
	}

	fmt.Println(t.GetDateTime())

	fmt.Printf("OK:%d,ERR:%d\n", ok, err)

Test Result:

PS F:\yjh201960613\yjhgo\golib\test> go run main.go
2021-06-30 15:38:45
2021-06-30 15:38:51
OK:10000000,ERR:0
```


```list

type TestA struct {
	Name  string
	Age   int
	IsMan bool
}
type TestAOutput struct {
	Name string
	Age  int
}

func main() {

	a := []int{1, 4, 3, 2, 5, 7, 9, 12, 45, 77, 98}
	b := New[int](a).Where(func(e *int) bool { return *e%2 == 0 }).ToList()
	f := New[Test](d).Where(func(e *Test) bool { return strings.Contains(e.Name, "A") }).ToList()


	ds := []TestA{
		{
			Name:  "yjh1",
			Age:   18,
			IsMan: true,
		}, {
			Name:  "yjh2",
			Age:   19,
			IsMan: false,
		}, {
			Name:  "yjh3",
			Age:   20,
			IsMan: true,
		},
	}

	ds1 := list.New2[TestA, string](ds).Where(func(e *TestA) bool { return e.Age > 18 }).Select(func(e *TestA) string { return e.Name })
	fmt.Println(ds1)

	ds2 := list.New2[TestA, TestAOutput](ds).Where(func(e *TestA) bool { return e.IsMan }).Select(func(e *TestA) TestAOutput {
		return TestAOutput{
			e.Name, e.Age,
		}
	})
	fmt.Println(ds2)
}

```
